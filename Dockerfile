FROM node:12-alpine

WORKDIR /usr/src/app

COPY package.json ./
COPY ./src ./src

RUN npm install

EXPOSE 8080
CMD [ "node", "./src/app.js" ]