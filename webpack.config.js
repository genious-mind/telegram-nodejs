const path = require('path');
const NodemonPlugin = require('nodemon-webpack-plugin');

module.exports = {
    target: 'node',
    mode: 'development',
    devtool: 'false',
    entry: './src/app',
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: 'dist.js',
    },
    resolve: {
        extensions: ['.mjs', '.js', '.ts', '.json', '.yaml'],
    },
    module: {
        rules: [
            {
                test: /\.(js|ts)?$/,
                exclude: /node_modules/,
                loader: ["babel-loader", "eslint-loader"],
            },
            /********************** WORKAROUND for WEBPACK error **********************/
            {
                test: /\.mjs$/,
                include: /node_modules/,
                type: "javascript/auto"
            }
        ],
    },
    plugins: [
        new NodemonPlugin()
    ]
};