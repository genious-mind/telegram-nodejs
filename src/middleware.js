import getUuid from 'uuid-by-string';

export default (ctx, next) => {
  ctx.state.accountCode = getUuid(ctx.from.id.toString());
  return next(ctx);
};
