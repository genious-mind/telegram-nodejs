import axios from 'axios';

export default class RestService {

  constructor(server) {
    this.axios = axios.create({
      baseURL: server,
    });
  }

  async getMatch({replyWithHTML, state, i18n}) {
    try {
      const response = await this.axios.get(`/${state.accountCode}`);
      return replyWithHTML(RestService.toMatchTable(response.data, i18n));
    } catch (e) {
      console.error(e);
    }
  }

  async createMatch({replyWithHTML, state, i18n}) {
    try {
      const response = await this.axios.post(`/${state.accountCode}`, {
        maxDigit: 10,
        numberLength: 4,
        style: 'NUMBER',
      });
      return replyWithHTML(RestService.toMatchTable(response.data, i18n));
    } catch (e) {
      console.error(e);
    }
  }

  static toMatchTable(match, i18n) {
    return (
      `<pre>
      ${RestService.getTableHeader(i18n)}
      +-----------+-----------+-----------+
      ${RestService.getTableBody(match.attempts)}
      </pre>`
    );
  }

  static getTableHeader(i18n) {
    const header = {
      number: i18n.t('table.header.number'),
      exact: i18n.t('table.header.exact'),
      present: i18n.t('table.header.present'),
    };

    return `| ${header.number} | ${header.exact} | ${header.present} |`;
  }

  static getTableBody(attempts) {
    return attempts.map(RestService.getAttemptRow).join('\n');
  }

  static getAttemptRow(attempt) {
    return `| ${attempt.number} | ${attempt.exactDigits} | ${attempt.containedDigits} |`;
  }

}
