import path from 'path';
import Telegraf from 'telegraf';
import TelegrafI18n, {match} from 'telegraf-i18n';
import {middlewareAccount} from './middleware';
import RestService from './RestService';

const i18n = new TelegrafI18n({
  defaultLanguage: 'en',
  allowMissing: true,
  directory: path.resolve(__dirname, 'locale'),
});

const restService = new RestService(process.env.MASTERMIND_SERVER_PATH);

const bot = new Telegraf(process.env.TELEGRAM_BOT_TOKEN);
bot.use(i18n.middleware());
bot.use(Telegraf.log());
bot.use(middlewareAccount);

bot.start(({reply, state, i18n}) => reply(i18n.t('command.start')));

bot.hears(match('command.create.name'), (ctx) => restService.createMatch(ctx));
bot.hears(match('command.get.name'), (ctx) => restService.getMatch(ctx));

bot.startPolling();
