module Response {

  export interface Match {

    code: string;
    accountCode: string;
    status: string;
    endDate: string;
    startDate: string;
    maxDigit: number,
    numberLength: number;
    style: string;
    number: number[];
    attempts: Response.Attempt[];

  }

  export interface Attempt {

    number: number[];
    containedDigits: number;
    exactDigits: number;
    date: string;

  }

}
